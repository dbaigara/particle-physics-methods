# Particle Physics Methods

Training for undergrads and grad students who are interested in particle physics research and want to learn how to use the computational tools common in the field.

![Event display](figures/event-display.png)
<br>

## Lessons
Getting started with research in particle physics is hard. We provide a series of **problem-based** tutorials to gain an introduction to the tools most commonly used. Each lesson has:
- An introduction with instructions on downloading the tool
- Examples of situations in which the tool may be useful with simple code samples
- Homework problems and solutions for self-checking understanding

The lesson order is:
1. [Python](python-tutorial)
2. [ROOT](root-tutorial)
3. [Machine learning with TMVA](tmva-tutorial)
4. [Fitting with RooFit](roofit-tutorial)
5. [Simulation](simulation-method-tutorial)

## About the authors
[Ben Carlson](https://www.westmont.edu/people/ben-carlson-phd) is an Assistant Professor of Physics at Westmont College. <br>
[Steve Roche](https://www.linkedin.com/in/stephen-roche-801125181) is a Medical Student at Saint Louis University with a background in particle physics.

## Questions, comments, or concerns?
Feel free to let us know of any recommendations via [email](mailto:str55@pitt.edu) or on the [issues tab on GitLab](https://gitlab.com/stroche/particle-physics-methods/-/issues)