# Running simulations with Madgraph, Delphes, and Pythia

## Getting Started
- Run *setupSimulations.sh*
- This will install Madgraph, Delphes, and Pythia here and make them all talk to each other
- Do this **every time** you want to run simulations to initialize the software. It will only download them all the first time, but it's needed every time to initialize them.

### Making Pileup
run *createPileup.sh*

## Simple Example
Minimal running example with analysis script can be found in [dijet-example](dijet-example)

## Second Example
In [z_to_tau_tau_example](z_to_tau_tau_example), a Z boson is produced in proton-proton collisons. The Z-boson is then told to decay to taus. The Z-boson mass is reconstructed using the invariant mass of the taus