# Creating and analyzing dijet sample

## Running madgraph
Open Madgraph with
```bash
../MG5_aMC_v3_3_2/bin/mg5_aMC 
```
Now run
```
generate p p > j j
output dijet-process
launch
```

We'll use the default options, so for the next two prompts just enter
```
0
0
```
and Madgraph should start chugging along, creating 10,000 dijet events. Once it's done, exit Madgraph.

## Running Delphes
Madgraph made for us an LHE file with these events. First, unzip it.
```bash
gunzip dijet-process/Events/run_01/unweighted_events.lhe.gz
```

Now, let's run Delphes. If you don't want to use pileup run
```
../delphes/DelphesPythia8 ./delphes_card_ATLAS.tcl ./pythiaCard.cmnd dijetOutput.root
```
If you do want to use pileup run
```
../delphes/DelphesPythia8 ./delphes_card_ATLAS_PileUp.tcl ./pythiaCard.cmnd dijetOutput.root
```


This takes as input
1. The card describing our detector (here I use ATLAS)
2. The card telling Pythia what to do (here I use a very basic card)
3. The name of our output file

This will produce 1000 events in *dijetOutput.root*

## Analysis
The script *plotEnergies.C* takes an event and plots the calorimeter towers on a grid with energies corresponding to different colors. The reconstructed and truth jets are overlaid on top.
Run it with
```bash
root plotEnergies.C
```

![energies_152.png](energies_152.png)

![energies_152_psr.png](energies_152_psr.png)

### Instructions

#### Dependencies
Delphes writes out a ROOT TTree that is a little different than ones to which you may be used. First of all, rather than saving floats like you've learned, it saves lists of custom objects. For instance, the class *Jet* is defined by Delphes. So in the *Jet* branch, each event has a list of *Jet* objects.

In order to use these custom objects, you need to
```c++
#include "classes/DelphesClasses.h"
```

Furthermore, because the Delphes tree is in such a non-friendly format, the Delphes developers have written some classes, *ExRootAnalysis*, in order to help out with reading them. We'll be using these, so we'll need to

```c++
#include "external/ExRootAnalysis/ExRootTreeReader.h"
```

Therefore, at the top of each analysis file you'll need
```c++
#ifdef __CLING__
R__LOAD_LIBRARY(libDelphes)
#include "classes/DelphesClasses.h"
#include "external/ExRootAnalysis/ExRootTreeReader.h"
#endif
```

and in the main function you'll need
```c++
gSystem->Load("libDelphes");
```

Importantly, these two classes are not available by default. You'll need to run [setupSimulations.sh](../setupSimulations.sh) each time you open a terminal in which you want to do simulation stuff. This runs [DelphesEnv.sh](../DelphesEnv.sh), setting up the environment. This step is prone to bugs, because the default *delphes/DelphesEnv.sh* is flawed. I've written [my own](../DelphesEnv.sh), which is automatically copied into the *delphes* directory and ran. It should work, but it may break in the future.

#### Using ExRootAnalysis
First, load in the TFile and TTree as you normally would.
```c++
TFile *f = new TFile("dijetOutput.root");
TTree *t = (TTree*) f->Get("Delphes");
```

Next, set up an *ExRootTreeReader*, along with *TClonesArrays* for the Branches you wish to use in your analysis.

For instance, if you want to use calorimeter towers, photons, muons, and jets, you'll say

```c++
ExRootTreeReader *treeReader = new ExRootTreeReader(t);
TClonesArray *branchTower = treeReader->UseBranch("Tower");
TClonesArray *branchJet = treeReader->UseBranch("Jet");
TClonesArray *branchPhoton = treeReader->UseBranch("Photon");
TClonesArray *branchMuon = treeReader->UseBranch("Muon");
```

Next, in order to get an event, you can call it like so. Here, I fetch the 153rd event
```c++
treeReader->ReadEntry(153);
```

Now, *branchTower*, *branchJet*, *branchPhoton*, and *branchMuon* have been filled with the relevant data points from that event.

If we want to loop over the jets in that event and print out their transverse momenta, now we can do
```c++
for (int i=0; i < branchJet->GetEntries(); ++i)
{
	Jet *jet = (Jet*) branchJet->At(i);
	std::cout << Jet->PT << std::endl;
}
```

One important thing to be able to do is convert Delphes objects to TLorentzVector objects. Here, I grab event 12 and find the four-vector for the third photon

```c++
treeReader->ReadEntry(12);
Photon *p = (Photon*) branchPhoton->At(2);
TLorentzVector photonFourVector = p->P4();
```