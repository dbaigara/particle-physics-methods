TestArea=$PWD
PileupFile=$TestArea/GeneratePileup/MinBias.pileup
if [ -f "$PileupFile" ]; then
    echo "$PileupFile exists."
else
    echo "$PileupFile does not exist. I'll make it."
    cd GeneratePileup
    ./generatePU.sh
fi
