#ifdef __CLING__
R__LOAD_LIBRARY(libDelphes)
#include "classes/DelphesClasses.h"
#include "external/ExRootAnalysis/ExRootTreeReader.h"
#endif


void analysis()
{
  gSystem->Load("libDelphes");

  TChain chain("Delphes");
  chain.Add("z_to_tau_tau_output.root");

  ExRootTreeReader *treeReader = new ExRootTreeReader(&chain);
  TClonesArray *branchJet = treeReader->UseBranch("Jet");
  
  int nEvents = treeReader->GetEntries();


  // setup histogram
  TH1F *diTau_mass = new TH1F("ditaumass", "",
			    30, 20, 150);
  
  for(int n=0; n < nEvents; ++n)
    {
      TLorentzVector METVector;

      treeReader->ReadEntry(n);
      int nJets = branchJet->GetEntries();
      std::vector<TLorentzVector> taus;
      
      for (int i=0; i < nJets; ++i)
	{
	  Jet *jet = (Jet*) branchJet->At(i);

	  if (jet->TauTag) // only use check if jet is tau
	    {
	      taus.push_back(jet->P4());
	    }
	}
      if (taus.size() == 2)
	{
	  float invariant_mass = (taus[0] + taus[1]).M();
	  diTau_mass->Fill(invariant_mass);
	}
    }
  TCanvas *c1 = new TCanvas("c1", "c1");
  diTau_mass->Draw();
  diTau_mass->GetXaxis()->SetTitle("m_{#tau #tau} [GeV]");
  diTau_mass->GetYaxis()->SetTitle("Events / 4.3 GeV");
  c1->SaveAs("m_tautau.png");
}
