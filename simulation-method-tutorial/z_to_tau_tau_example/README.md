# Creating and analyzing z->tau tau sample

# Method 1
## Running madgraph
Open Madgraph with
```bash
../MG5_aMC_v3_3_2/bin/mg5_aMC 
```
Now run
```
generate p p > z
output z_generation_process
launch
```

We'll use the default options, so for the next two prompts just enter
```
0
0
```
and Madgraph should start chugging along, creating 10,000 z events. Once it's done, exit Madgraph.

## Running Delphes
Madgraph made for us an LHE file with these events. First, unzip it.
```bash
gunzip z_generation_process/Events/run_01/unweighted_events.lhe.gz
```

Now, as before, we'll run Delphes and Pythia. Here, the main difference is that I added to [pythiaCard.cmnd](pythiaCard.cmnd) the instruction
```
! 4) force the Z to decay to tau tau
23:onMode = off   ! turn off all Z decays
23:onIfMatch = 15 15    ! turn decays to taus on back on
```
which forces a decay to taus. This uses the [PDG particle codes](https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf), that's what 23 (Z) and 15 (tau) mean.


If you don't want to use pileup run
```bash
../delphes/DelphesPythia8 ./delphes_card_ATLAS.tcl ./pythiaCard.cmnd z_to_tau_tau_output.root
```
If you do want to use pileup run
```
../delphes/DelphesPythia8 ./delphes_card_ATLAS_PileUp.tcl ./pythiaCard.cmnd z_to_tau_tau_output.root
```

This takes as input
1. The card describing our detector (here I use ATLAS)
2. The card telling Pythia what to do (here I use a very basic card)
3. The name of our output file

This will produce 1000 events in *z_to_tau_tau_output.root*

# Method 2
This method is very similar to method 1, except the Z->tau tau decay is done directly in madgraph.
## Running madgraph
Open Madgraph with
```bash
../MG5_aMC_v3_3_2/bin/mg5_aMC 
```
Now run
```
generate p p > z, (z > ta+ ta-)
output z_generation_process
launch
```

We'll use the default options, so for the next two prompts just enter
```
0
0
```
and Madgraph should start chugging along, creating 10,000 z-->tau tau events. Once it's done, exit Madgraph.

## Running Delphes
Madgraph made for us an LHE file with these events. First, unzip it.
```bash
gunzip z_generation_process/Events/run_01/unweighted_events.lhe.gz
```


This time, remove the following lines from [pythiaCard.cmnd](pythiaCard.cmnd). The Z's have already been decayed in Madgraph
```
! 4) force the Z to decay to tau tau
23:onMode = off   ! turn off all Z decays
23:onIfMatch = 15 15    ! turn decays to taus on back on
```

If you don't want to use pileup run
```bash
../delphes/DelphesPythia8 ./delphes_card_ATLAS.tcl ./pythiaCard.cmnd z_to_tau_tau_output.root
```

If you do want to use pileup run
```
../delphes/DelphesPythia8 ./delphes_card_ATLAS_PileUp.tcl ./pythiaCard.cmnd z_to_tau_tau_output.root
```

This takes as input
1. The card describing our detector (here I use ATLAS)
2. The card telling Pythia what to do (here I use a very basic card)
3. The name of our output file

This will produce 1000 events in *z_to_tau_tau_output.root*


## Analysis
Run *analysis.C*
This finds events with 2 tau-tagged jets and plots their invariant mass, which should be the mass of the Z boson.

![tau tau mass](m_tautau.png)