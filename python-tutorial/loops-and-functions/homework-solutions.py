# part 1

def euler(x, x0, y0, h):
    """
    Use euler's method to solve for y(x) given
    starting condition y(x0) = y0 for the differential equation
    y'(x) - y(x) = x^2 + 2x + 3
    Step size is h
    """

    def yprime(x_n, y_n):
        """
        solve for y'(x) in 
        y'(x) - y(x) = x^2 + 2x + 3
        for known x,y
        """
        # rearrange so that
        # y' = y + x^2 + 2x + 3
        return y_n + (x_n**2.0) + (2.0 * x_n) + 3.0


    def single_step(x_n, y_n):
        """
        Given y_n, return y_{n+1}
        """
        return y_n + (h * yprime(x_n, y_n))


    # start method with first step
    y_n = y0
    x_n = x0

    # continue until x_n --> x
    while x_n < x:
        y_n = single_step(x_n, y_n)
        x_n += h

    return x_n, y_n

print('Part 2')
# part 2
# solve for all possible
h = 0.0001
y0 = 6
x0 = 0
for x_val in [1.3, 3.7, 2.2, 0.9, 2.1]:
    x,y = euler(x_val, x0, y0, h)
    print('y(%s) = %s' %(x_val, y))

print('Part 3')
# part 3
x0 = 0
y0 = 0
x = 1.9
for h in [0.1, 0.05, 0.01, 0.001, 0.0001, 0.00001]:
    x_sol, y_sol = euler(x, x0, y0, h)
    print('h = %s' % h)
    print('y(%s) = %s' %(x, y_sol))
