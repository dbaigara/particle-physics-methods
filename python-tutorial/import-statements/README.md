# Import Statements and Code Modularization

When you're doing complex things with code, the amount you need to write can get very large, very quickly. Writing functions can help, but even so, sometimes it's just easier to have different blocks of code in different files.


For instance, let's say I have a very complex program, and all over it I find myself needing to find the average of three numbers, and I also find myself needing to reverse a string. So, I can write a file with this function in it like so

*helper_functions.py*
```python
def get_average(num1, num2, num3):
    return (num1 + num2 + num3) / 3.0

def reverse_string(input_string):
    return input_string[::-1]  # beleive it or not, this reverses a string
```

Now in another file I can *import* this file and call these functions like so

*program_file.py*
```python
import helper_functions

a = 12, b = 4, c = 9
avg = helper_functions.get_average(12,4,9)

some_string = "Hello world"
inverted_string = helper_functions.reverse_string(some_string)
```

If *helper_functions* is a lot to write, you can simply do

*program_file.py*
```python
import helper_functions as hf

a = 12, b = 4, c = 9
avg = hf.get_average(12,4,9)

some_string = "Hello world"
inverted_string = hf.reverse_string(some_string)
```

Note that I imported helper_functions "as hf", simply shortening how I call it

Finally, if I only want to import certain functions from a file I can do as follows:

*program_file.py*
```python
from helper_functions import get_average, reverse_string

a = 12, b = 4, c = 9
avg = get_average(12,4,9)

some_string = "Hello world"
inverted_string = reverse_string(some_string)
```

Just make sure that you don't have functions with conflicting names anywhere, or things get complex and buggy very quickly


## Outside Packages
Python has a very big community of developers who have written a lot of publicly available code. You can use The Python Package Index to use a lot of this public code.

For instance, we spent some time today looking at the Collatz Conjecture. But someone has a Python Package named collatz! You can find it [here](https://pypi.org/project/collatz/).

### Installing packages
Now I want to install this collatz package. To do so, in the terminal shell (not the Python shell) I can write

```bash
pip install collatz
```

Now in python I can use this other person's functions to do the same operations counts I did earlier without writing any code myself!
```python
import collatz
>>> collatz.stopping_time(4, total_stopping_time=True)
2
>>> collatz.stopping_time(15, total_stopping_time=True)
17
>>> collatz.stopping_time(67, total_stopping_time=True)
27
>>> collatz.stopping_time(1345, total_stopping_time=True)
114
```

# Homework
I'm doing some particle physics so I would like to describe particles using 4-vectors.
1. Find a package that implements Lorentz 4-vectors and install it.
2. Define 2 particles: one by it's (pT, eta, phi, mass), the other by its (energy, eta, phi, and momentum)
3. What is the invariant mass of the 2 particle system?