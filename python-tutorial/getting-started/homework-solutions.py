# problem 1
grades = {'Alpha': 'B',
          'Beta': 'A',
          'Gamma': 'A',
          'Delta': 'C',
          'Epsilon': 'D'}

# problem 2
ages = [12, 15, 12, 14, 13]

# problem 3
grades['Omega'] = 'A'
ages.append(14)

# problem 4
# you'll notice this is very inconvenient
# in the next lesson, you'll learn a better way to do it
average_age = 0.0
average_age += ages[0]
average_age += ages[1]
average_age += ages[2]
average_age += ages[3]
average_age += ages[4]
average_age += ages[5]
average_age /= len(ages)
print(average_age)

