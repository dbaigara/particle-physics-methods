# Python Tutorial

![Python logo](https://www.python.org/static/community_logos/python-logo-master-v3-TM.png)

This tutorial is for people just getting started using Python for scientific computing.

The modules should be followed in the following order
1. [Getting Started](getting-started)
2. [If statements](if-statements)
3. [Loops and functions](loops-and-functions)
4. [Import statements](import-statements)
5. [Numpy and Scipy](numpy-and-scipy-tutorial)

Each module has a short homework assignment at the end. The answer will be *homework-solution.py* in that module's directory