import ROOT
import numpy as np
from scipy.stats import norm

def normalize(hist):
    integral = 0.0
    for i in range(hist.GetNbinsX()+1):
        integral += hist.GetBinContent(i) * hist.GetBinWidth(i)
    hist.Scale(1.0 / integral)
    return hist


#---------------------------------------
# import dataset
#---------------------------------------
cb_data = np.genfromtxt('crystal-ball-dataset.txt')
x_min = min(cb_data)
x_max = max(cb_data)
#---------------------------------------
# RooFit model setup
#---------------------------------------

# set up variables, start with 1 for guesses for everything
m0 = ROOT.RooRealVar('m0', 'm0',                1, 1e-6, 1e6)
sigma = ROOT.RooRealVar('sigma', 'sigma',       1, 1e-6, 1e6)
alpha_l = ROOT.RooRealVar('alpha_l', 'alpha_l', 1, 1e-6, 1e6)
alpha_r = ROOT.RooRealVar('alpha_r', 'alpha_r', 1, 1e-6, 1e6)
eta_l = ROOT.RooRealVar('eta_l', 'eta_l',       1, 1e-6, 1e6)
eta_r = ROOT.RooRealVar('eta_r', 'eta_r',       1, 1e-6, 1e6)

x = ROOT.RooRealVar("x", "x", x_min, x_max)
crystal_ball = ROOT.RooCrystalBall("cb", "cb", x, m0,
                                   sigma,
                                   alpha_l, eta_l,
                                   alpha_r, eta_r)

#---------------------------------------
# fit model to data
#---------------------------------------
# make a RooDataSet from the random gaussian set from earlier
data = ROOT.RooDataSet.from_numpy({'x':cb_data}, [x])
crystal_ball.fitTo(data)

#---------------------------------------
# print results
# --------------------------------------
print('Fitted results')
print(m0)
print(sigma)
print(alpha_l)
print(alpha_r)
print(eta_l)
print(eta_r)



#---------------------------------------
# plot results
#---------------------------------------
# plot the gaussian data on a histogram
h = ROOT.TH1F("cb", "cb", 50, x_min, x_max)
for v in cb_data:
    h.Fill(v)
h = normalize(h)
    
# draw tgraph of fit line
fit_graph = ROOT.TGraph()
for v in np.linspace(x_min, x_max, 50000):
    x.setVal(v)
    fit_graph.SetPoint(fit_graph.GetN(), # set next point
                       v, # with this x
                       crystal_ball.getVal(x)) # and this y

# add legend
leg = ROOT.TLegend()
leg.AddEntry(h, 'Crystal Ball Data', 'lep')
leg.AddEntry(fit_graph, 'Fit', 'l')

# plot on canvas
c = ROOT.TCanvas()
h.Draw()
fit_graph.Draw('c, same')
leg.Draw('same')
c.SaveAs("crystal-ball.png")
