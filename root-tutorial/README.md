# ROOT Tutorial

![root logo](https://root.cern/assets/images/ROOT-image-share.png)

This tutorial will teach you how to use ROOT, the data analysis framework developed at CERN and used for pretty much all high energy physics. It's written in C++, but it has nice Python bindings, so pretty much all of this tutorial will be done in Python

## Lesson Order
1. [Installation](installation)
2. [Histogram Plotting](histogram-plotting)
3. [TGraph Plotting](tgraph-plotting)
4. [TProfile Plotting](tprofile-plotting)
5. [TTree Reading and Writing](ttree-reading-and-writing)
6. [TTree::Draw](ttree-draw)
7. [Homework](homework)
