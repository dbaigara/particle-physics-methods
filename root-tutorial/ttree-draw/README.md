# TTree::Draw Command

Often while performing an analysis you want to quickly check something by making a quick plot from a TTree. Here is where the TTree::Draw command comes in handy!

## Example
You have the dataset produced by [create-dataset.py](create-dataset.py).

![TBrowser screenshot](figures/tbrowser-screenshot.png)

To plot a histogram of pT:
```cpp
TFile *f = new TFile("toy-dataset.root", "READ");
TTree *t = (TTree*) f->Get("myTree");
t->Draw("pT");
```

This produces:

![pT plot](figures/pT-hist.png)


As you've seen, the first argument says what to plot. The second argument defines weighting. For instance, If you only want to see pT for events in the region |eta| > 2.5, you could do

```cpp
t->Draw("pT", "abs(eta) < 2.5")
```

This produces:

![pT eta plot](figures/pT-eta-region-hist.png)


## 2D Histograms
You can also draw 2D histograms. If I wanted to draw eta vs phi:
```cpp
t->Draw("eta:phi")
```

The third argument to TTree::Draw allows you to provide the usual draw options. If I wanted to draw the square root of eta vs phi, weight it by pT (for some reason), and plot it as a surface plot, I could do

```cpp
t->Draw("sqrt(eta):phi", "pT", "surf2");
```

This produces:

![surf plot](figures/surf-plot.png)


## Other options
There are lots of other draw options you can use with TTree::Draw. They are outlined in [ROOT's documentation](https://root.cern.ch/doc/master/classTTree.html#a73450649dc6e54b5b94516c468523e45)


## Save output
You can send the output of TTree::Draw to a histogram object fairly easily.
This can be done like so:

```cpp
TH1F *myHisto = new TH1F("hist1", "hist1", 50, 0, 200);
t->Draw("pT>>hist1");
```

Now **myHisto** has the output, and you can do stuff like:
```cpp
myHisto->SetStats(0);
myHisto->SetLineColor(kRed);
myHisto->SetLineWidth(4)
myHisto->SetFillColor(kCyan);

TCanvas *c = new TCanvas();
myHisto->Draw("hist");
c->SaveAs("ttree-draw-output.png");
```

And you'll have saved:

![figure](figures/ttree-draw-output.png)